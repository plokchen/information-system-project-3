package ch.ethz.globis.isk.domain.jpa;

import ch.ethz.globis.isk.domain.Person;
import ch.ethz.globis.isk.domain.Publication;

import java.util.HashSet;
import java.util.Set;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class JpaPublication extends Publication {

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Publication{");
        sb.append("key='").append(getId()).append('\'');
        sb.append(", title='").append(getTitle()).append('\'');
        sb.append(", year=").append(getYear());
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
    	if (this == o) return true;
        if (!(o instanceof Publication)) return false;

        Publication publication = (Publication) o;

        if (getId() != null ? !getId().equals(publication.getId()) : publication.getId() != null) return false;
        if (getTitle() != null ? !getTitle().equals(publication.getTitle()) : publication.getTitle() != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
    	int result = getId() != null ? getId().hashCode() : 0;
        result = 31 * result + (getTitle() != null ? getTitle().hashCode() : 0);
        return result;
    }
}