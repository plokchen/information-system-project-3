package ch.ethz.globis.isk.persistence;

import ch.ethz.globis.isk.domain.DomainObject;
import ch.ethz.globis.isk.util.Filter;
import ch.ethz.globis.isk.util.Operator;
import ch.ethz.globis.isk.util.Order;
import ch.ethz.globis.isk.util.OrderFilter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.BasicQuery;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public abstract class JpaDao<K extends Serializable, T extends DomainObject> implements Dao<K, T> {

    @Autowired
    @Qualifier("mongoOperations")
    MongoOperations mo;

    @Override
    public long countAllByFilter(Map<String, Filter> filterMap) {
        Query query = countQueryFromFilterMap(filterMap, null);
        return mo.count(query, getStoredClass());
    }

    @Override
    public long count() {
        Query query = countQueryFromFilterMap(null, null);
        return mo.count(query, getStoredClass());
    }

    @Override
    public Iterable<T> findAll() {
    	Query query = selectQueryFromFilterMap(null, null);
        return mo.find(query, getStoredClass());
    }

    @Override
    public T findOne(K id) {
//    	Map<String, Filter> filter = new HashMap<String, Filter>();
//    	filter.put("id", new Filter(Operator.EQUAL, id));
//    	Query query = selectQueryFromFilterMap(filter, null);
//      return mo.findOne(query, getStoredClass());
    	return mo.findById(id, getStoredClass());
    }

    @Override
    public T findOneByFilter(Map<String, Filter> filterMap) {
    	Query query = selectQueryFromFilterMap(filterMap, null);
        return mo.findOne(query, getStoredClass());
    }

    @Override
    public Iterable<T> findAllByFilter(Map<String, Filter> filterMap) {
    	Query query = selectQueryFromFilterMap(filterMap, null);
        return mo.find(query, getStoredClass());
    }

    @Override
    public Iterable<T> findAllByFilter(Map<String, Filter> filterMap, int start, int size) {
    	Query query = selectQueryFromFilterMap(filterMap, null).skip(start).limit(size);
        return mo.find(query, getStoredClass());
    }

    @Override
    public Iterable<T> findAllByFilter(Map<String, Filter> filterMap,
                                       List<OrderFilter> orderList,
                                       int start, int size) {
//        CriteriaQuery<T> query = selectQueryFromFilterMap(filterMap, orderList);
//
//        return em.createQuery(query).setFirstResult(start).setMaxResults(size).getResultList();
    	Query query = selectQueryFromFilterMap(filterMap, orderList).skip(start).limit(size);
    	return mo.find(query, getStoredClass());
    }

    @Override
    public Iterable<T> findAllByFilter(Map<String, Filter> filterMap,
                                       List<OrderFilter> orderList) {
        Query query = selectQueryFromFilterMap(filterMap, orderList);
        return mo.find(query, getStoredClass());
    }

    @Override
    public <S extends T> Iterable<S> insert(Iterable<S> entities) {
        for(S entity : entities) {
            insert(entity);
        }
        return entities;
    }

    @Override
    public <S extends T> S insert(S entity) {
//        em.persist(entity);
    	mo.insert(entity);
        return entity;
    }

    protected abstract <S extends T> Class<S> getStoredClass();

    protected List<T> queryByReferenceIdOrderByYear(String entity, String referenceName, String referenceId) {
//        String idField = referenceName + "Id";
//        String findAuthorsQuery = "Select p from %s p JOIN p.%s e " +
//                "WHERE e.id = :%s ORDER BY p.year ASC";
//        findAuthorsQuery = String.format(findAuthorsQuery, entity, referenceName, idField);
//        Query query = em.createQuery(findAuthorsQuery);
//        query.setParameter(idField, referenceId);
//        return query.getResultList();
    	//TODO
//    	Iterable<T> tmpIt = findAll();
//    	for()
//    	Map<String, Filter> map = new HashMap<String, Filter>();
//    	map.put("referenceName", new Filter(Operator.EQUAL, ));
//    	findOneByFilterMap();
    	BasicQuery query = new BasicQuery("{" + referenceName +".id : { $eq : " + referenceId + " } }");
    	query.with(new Sort(Sort.Direction.ASC, "year"));
    	try {
			return (List) mo.find(query, Class.forName(entity));
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return null;
		}
    }

    private Query countQueryFromFilterMap(Map<String, Filter> filterMap,
                                                        List<OrderFilter> orderList) {
        return selectQueryFromFilterMap(filterMap, null);
    }

    private Query selectQueryFromFilterMap(Map<String, Filter> filterMap,
                                                      List<OrderFilter> orderList) {
//        CriteriaBuilder cb = em.getCriteriaBuilder();
//        CriteriaQuery<T> query = cb.createQuery(getStoredClass());
//        Root <T> entity = query.from(getStoredClass());
//        query = query.select(entity);
//
//        //add filter values
//        Predicate[] restrictions = constructRestrictions(filterMap, entity, cb);
//        if (restrictions != null) {
//            query.where(restrictions);
//        }
//
//        //add order fields
//        List<javax.persistence.criteria.Order> criteriaOrderList = constructOrderList(orderList, entity, cb);
//        if (criteriaOrderList != null) {
//            query.orderBy(criteriaOrderList);
//        }
//        return query;
    	
    	Criteria criteria = new Criteria();
    	Filter filter;
    	Query query = new Query();
    	
    	for(String s : filterMap.keySet()){
    		filter = filterMap.get(s);
    	
    		if(filter.getOperator() == Operator.EQUAL){
    			criteria = criteria.where(s).is(filter.getValue());
    		} else {
    			criteria = criteria.where(s).regex((String) filter.getValue());
    		}
    	}
    	
    	query = new Query(criteria);
    	
    	for(OrderFilter of : orderList){
    		if(of.getOrder() == Order.ASC) {
        		query = query.with(new Sort(Sort.Direction.ASC, of.getField()));
    		} else {
    			query = query.with(new Sort(Sort.Direction.DESC, of.getField()));
    		}

    	}
    	
    	return query; 
    }
}