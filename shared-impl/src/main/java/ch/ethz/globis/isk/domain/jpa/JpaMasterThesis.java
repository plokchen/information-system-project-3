package ch.ethz.globis.isk.domain.jpa;

import org.springframework.data.mongodb.core.mapping.Document;

import ch.ethz.globis.isk.domain.MasterThesis;
import ch.ethz.globis.isk.domain.School;

@Document
public class JpaMasterThesis extends MasterThesis {
}