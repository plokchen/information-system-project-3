package ch.ethz.globis.isk.domain.jpa;


import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import ch.ethz.globis.isk.domain.Article;
import ch.ethz.globis.isk.domain.JournalEdition;

@Document
public class JpaArticle extends Article {
	 
	@PersistenceConstructor
	 public JpaArticle(String cdrom, JournalEdition journalEdition, String pages){
		super.setCdrom(cdrom);
		super.setJournalEdition(journalEdition);
		super.setPages(pages);
	 }
}
