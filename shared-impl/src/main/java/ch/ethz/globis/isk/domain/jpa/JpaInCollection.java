package ch.ethz.globis.isk.domain.jpa;

import org.springframework.data.mongodb.core.mapping.Document;

import ch.ethz.globis.isk.domain.Book;
import ch.ethz.globis.isk.domain.InCollection;

@Document
public class JpaInCollection extends InCollection {
}