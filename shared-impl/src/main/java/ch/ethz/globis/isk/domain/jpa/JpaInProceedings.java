package ch.ethz.globis.isk.domain.jpa;


import org.springframework.data.mongodb.core.mapping.Document;

import ch.ethz.globis.isk.domain.InProceedings;
import ch.ethz.globis.isk.domain.Proceedings;

@Document
public class JpaInProceedings extends InProceedings {

}
